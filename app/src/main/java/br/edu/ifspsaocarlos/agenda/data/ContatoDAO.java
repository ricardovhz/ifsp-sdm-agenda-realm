package br.edu.ifspsaocarlos.agenda.data;


import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import br.edu.ifspsaocarlos.agenda.model.Contato;
import io.realm.Realm;
import io.realm.RealmResults;


public class ContatoDAO {

    public ContatoDAO(Context context) {
        Realm.init(context);
    }

    public List<Contato> buscaTodosContatos() {
        Realm realm = Realm.getDefaultInstance();
        try {
            RealmResults<Contato> results = realm.where(Contato.class).findAll();
            ArrayList<Contato> contatos = new ArrayList<>(results.size());
            for (Contato c : results) {
                contatos.add(realm.copyFromRealm(c));
            }

            return contatos;
        } finally {
            if (!realm.isClosed())
                realm.close();
        }
    }

    public List<Contato> buscaContato(String nome) {
        Realm realm = Realm.getDefaultInstance();
        try {
            RealmResults<Contato> results = realm.where(Contato.class).like("nome", nome + "*").findAll();
            ArrayList<Contato> contatos = new ArrayList<>(results.size());
            for (Contato c : results) {
                contatos.add(realm.copyFromRealm(c));
            }
            return contatos;
        } finally {
            if (!realm.isClosed())
                realm.close();
        }
    }

    public void salvaContato(final Contato c) {
        Realm realm = Realm.getDefaultInstance();
        try {

            if (c.getId() > 0) {
                final Contato contatoToUpdate = realm.where(Contato.class)
                        .equalTo("id", c.getId()).findFirst();
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        contatoToUpdate.setNome(c.getNome());
                        contatoToUpdate.setEmail(c.getEmail());
                        contatoToUpdate.setFone(c.getFone());
                    }
                });
            } else {
                realm.beginTransaction();
                Contato contatoToInsert = realm.createObject(Contato.class, getNextId());
                contatoToInsert.setNome(c.getNome());
                contatoToInsert.setFone(c.getFone());
                contatoToInsert.setEmail(c.getEmail());
                realm.insert(contatoToInsert);
                realm.commitTransaction();
            }
        } finally {
            if (realm.isInTransaction())
                realm.cancelTransaction();
            if (!realm.isClosed())
                realm.close();
        }
    }

    private long getNextId() {
        Realm realm = Realm.getDefaultInstance();
        try {
            Number res = realm.where(Contato.class).max("id");

            if (res == null)
                return 1L;
            else
                return (res.longValue() + 1L);
        } finally {
            if (!realm.isClosed())
                realm.close();
        }

    }


    public void apagaContato(Contato c) {
        Realm realm = Realm.getDefaultInstance();
        try {
            final Contato result = realm.where(Contato.class).equalTo("id", c.getId()).findFirst();
            if (result != null) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        result.deleteFromRealm();
                    }
                });
            }
        } finally {
            if (realm.isInTransaction())
                realm.cancelTransaction();
            if (!realm.isClosed())
                realm.close();
        }
    }
}
